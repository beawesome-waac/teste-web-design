# A proposta

Olá! Nós somos a We Are All Connected, uma startup localizada em São Paulo, com foco em Inovação e Tecnologia. Se você tem interesse em desenvolver o seu perfil e ser um futuro Web Designer, está na hora de dar o primeiro passo! Não esperamos perfeição para a realização do teste, apenas se esforce, faça e nos mostre.

Conheça o nosso site: https://waac.com.br

# Teste Web Design

O material disponibilizado foi prototipado pensando na versão Desktop, contudo, faz parte do teste deixar o projeto responsivo, condizendo com as técnicas do "mobile first". Ao receber esse link para a realização do teste via e-mail, você terá 07 dias para a conclusão do mesmo.

## Apresentação e Estimativa

Baixe os arquivos, faça a sua estimativa de horas do planejamento a ser realizado e envie um e-mail com o título **[Teste - Web Design] Estimativa** para beawesome@waac.com.br com o link do seu git e as suas informações pessoais. Nos envie também os seus projetos extracurriculares, caso tenha algum.

## Estratégia e Escopo de Tecnologias

Diante da finita quantidade de frameworks e bibliotecas de estilização open-source, fique a vontade para escolher as tecnologias a serem utilizadas. Você pode construir utilizando apenas HTML como também pode incrementar com JQuery, PHP, React, Angular, Vue, etc. A estruturação é por sua conta!

## Processo de avaliação

1. Estratégia e planejamento
2. Redibilidade e qualidade do código
3. Práticas, técnicas ou padrões

## Finalização

Ao finalizar o teste, enviar o e-mail com o título **[Teste - Web Design] Finalização** para beawesome@waac.com.br.
